//Дополнить код, чтоб программа выводила след текст в консоль.
//«I'm sleeping 0 ...I'm sleeping 1 ... I'm sleeping 2 ... main: I'm tired of waiting! I'm running finally main: Now I can quit.»
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay

import kotlin.system.measureTimeMillis

suspend fun delayedNumber1(): Int {
    delay(1000) // задержка на 1 секунду
    return 1
}

suspend fun delayedNumber2(): Int {
    delay(2000) // задержка на 2 секунды
    return 2
}

suspend fun main() = coroutineScope  {
    // последовательный вызов функций с задержкой
    val time1 = measureTimeMillis {
        val num1 =  delayedNumber1()
        val num2 =  delayedNumber2()
        val sum = num1 + num2
        println("Сумма чисел: $sum")
    }
    println("Время выполнения последовательного вызова: $time1 мс")

    // асинхронный вызов функций с задержкой
    val time2 = measureTimeMillis {

        val num1 = async { delayedNumber1() }

        val num2 = async { delayedNumber2() }
        var count = 0
        while (num1.isActive || num2.isActive) {
            println("I'm sleeping ${count++}...")
            delay(1000)
        }
        val sum = num1.await() + num2.await()
        println("main:  I'm tired of waiting! I'm running finally main: Now I can quit.")
        println("Сумма чисел: $sum")
    }
    println("Время выполнения асинхронного вызова: $time2 мс")
}
