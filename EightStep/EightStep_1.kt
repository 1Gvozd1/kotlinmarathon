//Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек. В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток
fun main() {
    Thread {
        while(true) {
            Thread.sleep(1000)
            println("World")
        }
    }.start()
    
    while(true) {
        Thread.sleep(2000)
        println("Hello,")
    }
}

