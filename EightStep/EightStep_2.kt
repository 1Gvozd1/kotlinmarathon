//Написать две функции с задержкой, которые будут возвращать 2 числа. В main написать блок кода, который будет суммировать вызов этих 2 функций, сначала написать последовательный вызов функций, вывести сумму и время работы, потом написать блок с асинхронным вызовом и сравнить время работы, обосновать время
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay

import kotlin.system.measureTimeMillis

suspend fun delayedNumber1(): Int {
    delay(1000) // задержка на 1 секунду
    return 1
}

suspend fun delayedNumber2(): Int {
    delay(2000) // задержка на 2 секунды
    return 2
}

suspend fun main() = coroutineScope  {
    // последовательный вызов функций с задержкой
    val time1 = measureTimeMillis {
        val num1 =  delayedNumber1()
        val num2 =  delayedNumber2()
        val sum = num1 + num2
        println("Сумма чисел: $sum")
    }
    println("Время выполнения последовательного вызова: $time1 мс")

    // асинхронный вызов функций с задержкой
    val time2 = measureTimeMillis {

        val num1 = async { delayedNumber1() }
        val num2 = async { delayedNumber2() }
        val sum = num1.await() + num2.await()

        println("Сумма чисел: $sum")
    }
    println("Время выполнения асинхронного вызова: $time2 мс")
}